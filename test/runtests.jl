using Test, MyAux, PaddedViews

@test foldlvec(=>, 1:3) == [1=>2, (1=>2)=>3]


@test pad([1; 2], 5) == [1; 2; 0; 0; 0]
@test pad([1 2;3 4], 3) == [1 2 0; 3 4 0; 0 0 0]
@test pad([1.; 2.], 5) == [1.;2.;0.;0.;0.]
@test pad([1; 2; 3], 2) == [1; 2]
@test pad([], 1) == Any[0]
@test pad([1 2; 3 4], 4, :left) == [0 0 0 0; 0 0 0 0; 0 0 1 2; 0 0 3 4]


@test getpkgid("PaddedViews") == UUID("5432bcbf-9aad-5242-b902-cca2824c8663")


@test Set(allsubtypes(Integer)) == Set([Bool, UInt64, Integer, UInt32, Signed, BigInt, UInt8, 
                                Int16, UInt128, Int8, Int128, Unsigned, UInt16, Int32, Int64])