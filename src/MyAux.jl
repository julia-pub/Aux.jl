module MyAux

export circumcentre

using Test
export @test, @testset

export newpkg

export compress_video, compress_video_folder


export foldlvec, pad
export @clearfun

using PkgTemplates

export subtypetree, allsubtypes, getpkgid

export toDD
#= To convert from AxisArray to DimArray
function toDD(fnamenoext::String)
    dict = load(fnamenoext * ".jld2")
    key = first(keys(dict))
    val = dict[key]
    dd = to(DimArray, val)
    dict_out = Dict(key=>dd)
    save(fnamenoext * "-dd.jld2", dict_out)
end
=#

function newpkg(name;user="julia-priv")
    t = Template(;
        user,
        dir="~/julia",
        host="gitlab.com",
        julia=v"1.6",
        plugins=[
            GitLabCI(;
                file=joinpath(@__DIR__, "pkg-gitlab-ci.yml"),
                extra_versions=["1.6"]),
            Documenter{GitLabCI}(),
            Develop(),
            !CompatHelper,
            !TagBot,
            Git(;ignore=["**/tmp*", "/docs/Manifest.toml"])
        ]
    )
    t(name)
end


# By Tim Holy
function getpkgid(name)
    project = Base.active_project()
    Base.project_deps_get(project, name)
end

"""
    circumcentre((x1, x2), (y1, y2), (z1, z2))
    circumcentre(xs::NTuple{3})
"""
circumcentre(xs::NTuple{3}) = circumcentre(xs...)
function circumcentre((x1, x2), (y1, y2), (z1, z2))
	out1 = (y2*z1^2 - (y1^2 + y2^2)*z2 + y2*z2^2 + x1^2*(-y2 + z2) + x2^2*(-y2 + z2) + x2*(y1^2 + y2^2 - z1^2 - z2^2)) /
  (2*(x2*(y1 - z1) + y2*z1 - y1*z2 + x1*(-y2 + z2)))
	out2 = 
 (2*(x1 - z1)*(y1^2 + y2^2 - z1^2 - z2^2) + 2*(y1 - z1)*(-x1^2 - x2^2 + z1^2 + z2^2))/
  (4*(-(y2*z1) + x2*(-y1 + z1) + x1*(y2 - z2) + y1*z2))
	
	return out1, out2
end


using PaddedViews
"""
    pad(x, s)

Zero-pad array `x` to size `s`. By default, pad right.

    pad(x, s, t::Symbol)

If `t` is `:right` (default), pad right. If it is `:left`, pad left.

    pad(x, s, init)

Make `x[1]` correspond to position defined by tuple `init` of 
the output. 
"""
function pad(x, s, t = :right)
        # resolve the position of `x[1]` in the output array
    if t === :right
        init = ktuple(1, length(s))
    elseif t === :left
        init = s .- size(x) .+ 1
    else
        error("Bad kind of pad")
    end
     
    return pad(x, s, init)
end
pad(x, s::Integer, init)  = pad(x, ktuple(s, ndims(x)), init)
pad(x, s, init :: Tuple) = PaddedView(0, x, s, init)

ktuple(v, n) = ntuple(_->v, n)


using InteractiveUtils:subtypes
"""
    allsubtypes(x)

Give a vector of all types, including `x`, that are a subtype of x
"""
allsubtypes(x::Type) = allsubtypes!([], x)
function allsubtypes!(s, x)
    push!(s, x)
    for st in subtypes(x)
        allsubtypes!(s, st)
    end
    return s
end

"""
    clearfun(f)

Clear methods of `f`.
"""
macro clearfun(fs)
    isdef = Expr(:isdefined, esc(fs))
    return quote
        if $isdef
            # delete the methods
            for m in methods($(esc(fs))); delete_method(m); end
            # delete the tests
            delete!(TESTS, $(esc(fs)))
        end
        nothing
    end
end

"""
    subtypetree(s; level, indent)

Print a nice view of the subtype tree structure of type `s``
"""
function subtypetree(t, level=1, indent=4)
   level == 1 && println(t)
   for s in subtypes(t)
     println(join(fill(" ", level * indent)) * string(s))
     subtypetree(s, level+1, indent)
   end
end


"""

    `foldlvec(f, v, init)`

Returns a vector with the partial results of `foldl`.
It's type unstable, returns a `::Vector{Any}`
"""
function foldlvec(f, v)
    len = length(v)
    if len <= 1; return eltype(v)[]; end

    out = Vector{Any}(undef, len-1)
    out[1] = f(v[1], v[2])

    for i in 3:len
        out[i-1] = f(out[i-2], v[i])
    end

    return out
end

include("Def.jl")
include("LazyLoading.jl")
include("compress_video.jl")

end # module
